function init() {
  $("#btn_start").click(start_analisys);
}

var url = "http://www2.eca.usp.br/jornadas/anais/";

function start_analisys(evt) {
  $(this).attr("disabled", true);
  $("#table_content").empty();
  $.getJSON("anais.php?url="+url)
      .done(onGetVolumes);
}

function onGetVolumes(data) {
  // console.log(data);
  // Loop volumes
  for (var volname in data) {
    var vol = data[volname];
    // Loop eixos
    for (let i = 0; i < vol.length; i++ ) {
      var eixo = vol[i];
      // Loop artigos
      for (addr in eixo['articles']) {
        var tr = $('<tr></tr>');
        var a  = $('<a></a>')
            .attr("href", "http://www2.eca.usp.br/jornadas/anais/" + eixo['addr'])
            .text(eixo['text']);
        var td_vol = $("<td></td>").text(volname);
        var td_eixo = $("<td></td>").append(a);
        tr.append(td_vol);
        tr.append(td_eixo);
        var a_art = $("<a></a>")
            .attr('href', url+volname+"/"+addr)
            .text(eixo['articles'][addr]['title']);
        var title = $("<td></td>").append(a_art);
        var author = $("<td></td>").text(eixo['articles'][addr]['author'])
        tr.append(title);
        tr.append(author);
        $("#table_content").append(tr);
      }// artigos
    }// eixos
  }// volumes
  $("#btn_start").attr("disabled", false);
}
