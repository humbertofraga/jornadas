#!/usr/bin/env bash

apt-get update
apt-get install -y git

cd /usr/local/apache2/htdocs/
php composer.phar --install-dir=. --filename=composer
php composer -n install
