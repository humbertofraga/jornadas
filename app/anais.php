<?php

require "vendor/autoload.php";
use PHPHtmlParser\Dom;

$cache_file = "jornadas.json";

if (file_exists($cache_file) && (filemtime($cache_file) > (time() - 60 * 60 * 24))) {
  echo file_get_contents($cache_file);
  exit;
}

$dom = new Dom;

$volumes = array(
  '1asjornadas' => array(),
  '2asjornadas' => array(),
  '3asjornadas' => array(),
  '4asjornadas' => array(),
  '5asjornadas' => array()
);
$key_volumes = array_keys($volumes);

$url = $_GET['url'];

$dom->load($url);
$as = $dom->find('a');

foreach ($as as $key => $item) {
  $tag = $item->getTag();
  $href = $tag->getAttribute('href')['value'];
  $text = $item->innerHtml;
  foreach ($key_volumes as $vol) {
    if (strpos($href, $vol) === 0) {
      // link is a jornada volume
      $articles = parseArticles($url.'/'.$href);
      $volume = array(
        'addr' => $href,
        'text' => $text,
        'articles' => $articles
      );
      $volumes[$vol][] = $volume;
      break;
    }
  }
}

function parseArticles($url) {
  $results = array();
  $dom_eixo = new Dom;
  $dom_eixo->load($url);

  $titulos = $dom_eixo->find('a.txt_artigo_titulo');
  foreach ($titulos as $key => $item) {
    $tag = $item->getTag();
    $href = $tag->getAttribute('href')['value'];
    error_log("href: ".$href);
    $results[$href]['title'] = $item->innerHtml;
  }

  $autores = $dom_eixo->find('a.txt_artigo_autor');
  foreach ($autores as $key => $item) {
    $tag = $item->getTag();
    $href = $tag->getAttribute('href')['value'];
    error_log("href: ".$href);
    $results[$href]['author'] = $item->innerHtml;
  }

  return $results;
}

echo json_encode($volumes);
file_put_contents($cache_file, json_encode($volumes), LOCK_EX);

?>
