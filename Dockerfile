FROM php:7-apache

RUN apt-get update && \
    apt-get install -y curl zip unzip && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD app/ /var/www/html/

RUN cd /var/www/html && \
    composer install --no-interaction

EXPOSE 80
