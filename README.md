# Jornadas

Extrai dados dos Anais Eletrônicos das Jornadas Internacionais de Histórias em Quadrinhos (http://www2.eca.usp.br/jornadas/anais/)

Este é um simples projeto PHP+Javascript que navega pelos Anais das Jornadas Internacionais de Histórias em Quadrinhos e extrai de lá informações sobre os volumes, eixos, artigos e autores.

O projeto pode ser instanciado em um servidor executando PHP 7.0 ou mais recente, e utiliza o composer (https://getcomposer.org) para instalar a dependência php-html-parser (https://github.com/paquettg/php-html-parser).

O projeto conta também com um Dockerfile que permite construir uma imagem Docker que executa o projeto.

## Instruções para Docker

 - Instalar o Docker (https://docs.docker.com/docker-for-windows/install/).
 - Construir a imagem:
```bash
docker build -t jornadas:latest .
```
 - Iniciar a imagem:
```bash
docker run --rm -p 80:80 jornadas:latest
```
 - Acessar no navegador: http://localhost